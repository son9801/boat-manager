<?php

use App\Http\Controllers\BoatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/boats', [BoatController::class, 'index'])->name('boats.index');
Route::post('/boats', [BoatController::class, 'store'])->name('boats.store')->middleware('auth');
Route::get('/boats/create', [BoatController::class, 'create'])->name('boats.create')->middleware('auth');
Route::get('/boats/show/{id}', [BoatController::class, 'show'])->name('boats.show')->middleware('auth');
Route::put('/boats/update/{id}', [BoatController::class, 'update'])->name('boats.update');
Route::get('/boats/delete/{id}', [BoatController::class, 'delete'])->name('boats.delete')->middleware('auth');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
