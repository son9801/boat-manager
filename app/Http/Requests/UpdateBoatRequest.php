<?php

namespace App\Http\Requests;

use App\Models\Boat;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBoatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = $this->route('id');
        return [
            'name' => 'required|regex:/^[\pL\s.]+$/u|unique:boat,name,' . $id,
            'type'=>'required|regex:/^[\pL\s.]+$/u',
            'length'=>'required|gt:0',
            'beam'=>'required|gt:0',
            'material'=>'required|regex:/^[\pL\s.]+$/u',
            'color'=>'required|regex:/^[\pL\s.]+$/u',
            'cost'=>'required|gt:0',
        ];
    }
}
