<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBoatRequest;
use App\Http\Requests\UpdateBoatRequest;
use App\Models\Boat;
use Illuminate\Http\Request;

class BoatController extends Controller
{
    protected $boat;

    public function __construct(Boat $boat)
    {
        $this->boat = $boat;
    }

    public function index(Request $request)
    {
        $searchKey = $request->searchKey;

        if ($searchKey !== null) {
            $boats = $this->boat
                ->where('name', 'LIKE', '%' . $searchKey . '%')
                ->orWhere('type', 'LIKE', '%' . $searchKey . '%')
                ->orWhere('length', 'LIKE', '%' . $searchKey . '%')
                ->orWhere('beam', 'LIKE', '%' . $searchKey . '%')
                ->orWhere('material', 'LIKE', '%' . $searchKey . '%')
                ->orWhere('color', 'LIKE', '%' . $searchKey . '%')
                ->orWhere('cost', 'LIKE', '%' . $searchKey . '%')
                ->latest('id')
                ->paginate(10);
            $boats->appends(['searchKey' => $searchKey]);

            if ($boats->isEmpty()) {
                $message = "Không tìm thấy kết quả nào cho '{$searchKey}'";
                return view('boats.index', compact('boats', 'searchKey', 'message'));
            }
        } else {
            $boats = $this->boat->latest('id')->paginate(10);
        }

        return view('boats.index', compact('boats', ));
    }


    public function store(CreateBoatRequest $request)
    {
        $this->boat->create($request->all());

        return redirect()->route('boats.index');
    }

    public function create()
    {
        return view('boats.create');
    }

    public function show(string $id)
    {
        $boat = $this->boat->findorfail($id);

        return view('boats.update', compact('boat'));
    }

    public function update(UpdateboatRequest $request, string $id)
    {
        $boat = $this->boat->findorfail($id);
        $boat->update($request->all());

        return redirect()->route('boats.index');
    }

    public function delete(Request $request, string $id)
    {
        $boat = $this->boat->findorfail($id);
        $boat->delete();

        return redirect()->route('boats.index');
    }
}
