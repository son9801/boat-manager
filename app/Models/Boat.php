<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boat extends Model
{
    use HasFactory;

    protected $table = "boat";

    protected $fillable = [
        'name',
        'type',
        'length',
        'beam',
        'material',
        'color',
        'cost'];
}
