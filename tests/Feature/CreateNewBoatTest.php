<?php

namespace Tests\Feature;

use App\Models\Boat;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateNewBoatTest extends TestCase
{
    public function getCreateRoute()
    {
        return route('boats.store');
    }

    public function getCreateBoatViewRoute()
    {
        return route('boats.create');
    }

    /**
     * @test
     */
    public function authenticated_user_can_create_new_boat()
    {
        $this->actingAs(User::factory()->make());
        $boat = Boat::factory()->make()->toArray();
        $response = $this->post($this->getCreateRoute(), $boat);

        $response->assertStatus(302);
        $this->assertDatabaseHas('boat', $boat);
        $response->assertRedirect(route('boats.index'));
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_create_new_boat()
    {
        $boat = Boat::factory()->make()->toArray();
        $response = $this->post($this->getCreateRoute(), $boat);

        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_user_can_view_create_boat_form()
    {
        $this->actingAs(User::factory()->make());
        $response = $this->get($this->getCreateBoatViewRoute());

        $response->assertViewIs('boats.create');
    }

    /**
     * @test
     */
    public function unauthenticated_user_cant_see_create_boat_form()
    {
        $response = $this->get($this->getCreateBoatViewRoute());

        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_user_cant_create_new_boat_if_field_is_null_and_get_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $keys = ['name', 'type', 'length', 'beam', 'material', 'color', 'cost'];
        $boatData = array_fill_keys($keys, null);
        $boat = Boat::factory()->make($boatData)->toArray();
        $response = $this->from($this->getCreateBoatViewRoute())->post($this->getCreateRoute(), $boat);

        $response->assertRedirect($this->getCreateBoatViewRoute());
        $this->assertTrue(session()->has('errors'));
        $this->assertTrue(session('errors')
            ->has(['name',
                   'type',
                   'length',
                   'beam',
                   'material',
                   'color',
                   'cost'])
        );
    }

    /**
     * @test
     */
    public function authenticated_user_cant_create_new_boat_if_number_field_is_negative_and_get_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $boat = Boat::factory()->make(['length' => '-1'])->toArray();
        $response = $this->from($this->getCreateBoatViewRoute())->post($this->getCreateRoute(), $boat);

        $response->assertRedirect($this->getCreateBoatViewRoute());
        $this->assertTrue(session()->has('errors'));
        $this->assertTrue(session('errors')->has(['length']));
    }

    /**
     * @test
     */
    public function authenticated_user_cant_create_new_boat_if_name_is_duplicate_and_get_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $existingBoat = Boat::factory()->create();
        $boat = Boat::factory()->make(['name' => $existingBoat->name])->toArray();
        $response = $this->from($this->getCreateBoatViewRoute())->post($this->getCreateRoute(), $boat);

        $response->assertRedirect($this->getCreateBoatViewRoute());
        $this->assertTrue(session()->has('errors'));
        $this->assertTrue(session('errors')->has('name'));
    }

    /**
     * @test
     */
    public function authenticated_user_cant_create_new_boat_if_text_field_is_not_text_and_get_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $boat = Boat::factory()->make(['name' => '@','type'=>'*','material'=>'1','color'=>'1'])->toArray();
        $response = $this->from($this->getCreateBoatViewRoute())->post($this->getCreateRoute(), $boat);

        $response->assertRedirect($this->getCreateBoatViewRoute());
        $this->assertTrue(session()->has('errors'));
        $this->assertTrue(session('errors')->has('name','type','material','color'));
    }

}
