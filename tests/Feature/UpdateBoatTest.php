<?php

namespace Tests\Feature;

use App\Models\Boat;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateBoatTest extends TestCase
{
    public function getListBoatRoute()
    {
        return route('boats.index');
    }

    public function getUpdateBoatViewRoute($id)
    {
        return route('boats.show', ['id' => $id]);
    }

    public function putUpdateBoatRoute($id)
    {
        return route('boats.update', ['id' => $id]);
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_boat()
    {
        $this->actingAs(User::factory()->make());
        $boat = Boat::factory()->create();
        $modifiedBoatData = array_merge($boat->toArray(), ['name' => $boat->name.' Updated']);
        $response = $this->put($this->putUpdateBoatRoute($boat->id), $modifiedBoatData);

        $response->assertStatus(302);
        $this->assertDatabaseHas('boat', $modifiedBoatData);
        $response->assertRedirect($this->getListBoatRoute());
    }

    /**
     * @test
     */
    public function authenticated_user_can_view_update_boat_form()
    {
        $this->actingAs(User::factory()->make());
        $boat = Boat::factory()->create();
        $response = $this->get($this->getUpdateBoatViewRoute($boat->id));

        $response->assertViewIs('boats.update');
        $response->assertSee(['name', 'type', 'length', 'beam', 'material', 'color', 'cost']);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cant_see_update_boat_form()
    {
        $boat = Boat::factory()->create();
        $response = $this->get($this->getUpdateBoatViewRoute($boat->id));

        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_user_cant_update_new_boat_if_field_is_null_and_get_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $boat = Boat::factory()->create();
        $keys = ['name', 'type', 'length', 'beam', 'material', 'color', 'cost'];
        $modifiedBoatData = array_fill_keys($keys, null);
        $response = $this->from($this->getUpdateBoatViewRoute($boat->id))->put($this->putUpdateBoatRoute($boat->id), $modifiedBoatData);

        $response->assertRedirect($this->getUpdateBoatViewRoute($boat->id));
        $this->assertTrue(session()->has('errors'));
        $this->assertTrue(session('errors')
            ->has(['name',
                   'type',
                   'length',
                   'beam',
                   'material',
                   'color',
                   'cost'])
        );
    }

    /**
     * @test
     */
    public function authenticated_user_cant_update_boat_if_number_field_is_negative_and_get_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $boat = Boat::factory()->create();
        $modifiedBoatData = array_merge($boat->toArray(), ['name' => $boat->name . 'Updated', 'length' => '-1', 'beam' => '-1', 'cost' => '-1']);
        $response = $this->from($this->getUpdateBoatViewRoute($boat->id))->put($this->putUpdateBoatRoute($boat->id), $modifiedBoatData);

        $response->assertRedirect($this->getUpdateBoatViewRoute($boat->id));
        $this->assertTrue(session()->has('errors'));
        $this->assertTrue(session('errors')
            ->has(['length',
                   'beam',
                   'cost'])
        );
    }

    /**
     * @test
     */
    public function authenticated_user_cant_update_boat_if_name_is_duplicate_and_get_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $existingBoat = Boat::factory()->create();
        $boat = Boat::factory()->create();
        $modifiedBoatData = array_merge($boat->toArray(), ['name' => $existingBoat->name]);
        $response = $this->from($this->getUpdateBoatViewRoute($boat->id))->put($this->putUpdateBoatRoute($boat->id), $modifiedBoatData);

        $response->assertRedirect($this->getUpdateBoatViewRoute($boat->id));
        $this->assertTrue(session()->has('errors'));
        $this->assertTrue(session('errors')->has('name'));
    }

    /**
     * @test
     */
    public function authenticated_user_cant_update_boat_if_text_field_is_not_text_and_get_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $boat = Boat::factory()->create();
        $modifiedBoatData = array_merge($boat->toArray(), ['name' => '@', 'type' => '*', 'material' => '1', 'color' => '1']);
        $response = $this->from($this->getUpdateBoatViewRoute($boat->id))->put($this->putUpdateBoatRoute($boat->id), $modifiedBoatData);

        $response->assertRedirect($this->getUpdateBoatViewRoute($boat->id));
        $this->assertTrue(session()->has('errors'));
        $this->assertTrue(session('errors')->has('name','type','material','color'));
    }
}
