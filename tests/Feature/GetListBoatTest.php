<?php

namespace Tests\Feature;

use App\Models\Boat;
use Illuminate\Console\View\Components\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListBoatTest extends TestCase
{
    public function getListBoatRoute($searchKey)
    {
        return route('boats.index',['searchKey' => $searchKey]);
    }

    /**
     * @test
     */
    public function user_can_get_all_task()
    {
        $boat = Boat::factory()->create();
        $response = $this->get($this->getListBoatRoute(''));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('boats.index');
        $response->assertSee($boat->name);
    }


    /**
     * @test
     */
    public function user_can_get_search_result_with_keyword()
    {
        $boat = Boat::factory()->create();
        $response = $this->get($this->getListBoatRoute($boat->name));

        $response->assertStatus(200);
        $response->assertViewIs('boats.index');
        $response->assertSeeText($boat->name);
    }


    /**
     * @test
     */
    public function user_cant_get_search_result_with_null_keyword()
    {
        $boat = Boat::factory()->create();
        $response = $this->get($this->getListBoatRoute(''));

        $response->assertViewIs('boats.index');
    }

    /**
     * @test
     */
    public function user_can_get_search_results_and_error_message_with_invalid_keyword()
    {
        $invalidKeyword = '@';
        $response = $this->get($this->getListBoatRoute($invalidKeyword));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('boats.index');
        $response->assertSeeText('Không tìm thấy kết quả nào cho \'' . $invalidKeyword . '\'');
    }
}
