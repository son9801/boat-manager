<?php

namespace Tests\Feature;

use App\Models\Boat;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteBoatTest extends TestCase
{
    public function deleteBoatRoute($id)
    {
        return route('boats.delete', ['id' => $id]);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete_boat()
    {
        $this->actingAs(User::factory()->make());
        $boat = Boat::factory()->create();
        $response = $this->get($this->deleteBoatRoute($boat->id));

        $response->assertStatus(302);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cant_delete_boat()
    {
        $boat = Boat::factory()->create();
        $response = $this->get($this->deleteBoatRoute($boat->id));

        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function authenticated_user_cant_delete_boat_if_boat_not_exist()
    {
        $this->actingAs(User::factory()->make());
        $boatId = -1;
        $response = $this->get($this->deleteBoatRoute($boatId));

        $response->assertStatus(404);
    }
}
