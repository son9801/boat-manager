@extends('layouts.app')

@section('content')

    <div class="container">
        @if(isset($message))
            <div class="alert alert-warning">
                {{ $message }}
            </div>
        @endif
        <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="mb-0">TASK LIST</h4>
                        <div class="d-flex justify-content-center align-items-center flex-grow-1">
                            <form class="d-flex" method="GET" action="{{ route('boats.index') }}">
                                <div class="input-group">
                                    <input class="form-control" type="search" aria-label="Search"
                                           name="searchKey" value="{{ request('searchKey') }}">
                                    <button class="btn btn-outline-success" type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                        <div class="d-flex justify-content-end align-items-center">
                            <a href="{{ route('boats.create') }}" class="btn btn-primary">Add</a>
                        </div>
                    </div>
                </div>

            <div class="card-body">
                <div class="row justify-content-center">
                    <table class="table table-strips">
                        <tr>
                            <th style="width: 10%;">ID</th>
                            <th style="width: 30%;">Name</th>
                            <th style="width: 10%;">Type</th>
                            <th style="width: 10%;">Length</th>
                            <th style="width: 10%;">Beam</th>
                            <th style="width: 10%;">Material</th>
                            <th style="width: 10%;">Color</th>
                            <th style="width: 10%;">Cost</th>
                            <th>Action</th>
                        </tr>
                        @foreach($boats as $boat)
                            <tr>
                                <th> {{$boat->id}}</th>
                                <th> {{$boat->name}}</th>
                                <th> {{$boat->type}}</th>
                                <th> {{$boat->length}}</th>
                                <th> {{$boat->beam}}</th>
                                <th> {{$boat->material}}</th>
                                <th> {{$boat->color}}</th>
                                <th> {{$boat->cost}} USD</th>
                                <th>
                                    <div class="btn-group">
                                        <a href="{{ route('boats.show', $boat->id) }}"
                                           class="btn btn-warning">Update </a> <br/>
                                        <span class="mx-1"></span>
                                        <a href="{{ route('boats.delete', $boat->id) }}" onclick="return confirm('Are you sure you want to delete this boat?')"
                                           class="btn btn-danger">Delete</a>
                                    </div>
                                </th>
                            </tr>
                        @endforeach
                    </table>
                    {{$boats->links()}}
                </div>
            </div>
        </div>

    </div>
@endsection
