@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Create Boat</h2>
                <div class="d-flex justify-content-end align-items-center">
                    <a href="{{ route('boats.index') }}" class="btn btn-primary">Back</a>
                </div>
                @if ($errors->any())
                    <div class="alert alert-warning">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                <form action="{{ route('boats.store') }}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" name="name" placeholder="name...">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" name="type" placeholder="type...">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="number" class="form-group" name="length" placeholder="length...">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="number" class="form-group" name="beam" placeholder="beam...">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" name="material" placeholder="material...">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" name="color" placeholder="color...">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="number" class="form-group" name="cost" placeholder="cost...">
                        </div>
                    </div>
                        <button class="btn btn-success">submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
