@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Edit boat</h2>
                @if ($errors->any())
                    <div class="alert alert-warning">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                <form action="{{ route('boats.update', $boat->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" value="{{ $boat->name }}" name="name">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" value="{{ $boat->type }}" name="type">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="number" class="form-group" value="{{ $boat->length }}" name="length">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="number" class="form-group" value="{{ $boat->beam }}" name="beam">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" value="{{ $boat->material }}" name="material">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" value="{{ $boat->color }}" name="color">
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="number" class="form-group" value="{{ $boat->cost }}" name="cost">
                        </div>
                    </div>
                    <button class=" btn btn-success">submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
