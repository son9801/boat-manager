<?php

namespace Database\Factories;

use App\Models\Boat;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Boat>
 */
class BoatFactory extends Factory
{
    protected $model = Boat::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'type' => $this->faker->randomElement(['Đánh cá', 'Chở hàng', 'Du lịch']),
            'length'=>$this->faker->numberBetween(50,200,100 ),
            'beam'=>$this->faker->numberBetween(50, 200),
            'material' => $this->faker->randomElement(['Gỗ', 'Sắt', 'Thép']),
            'color' => $this->faker->randomElement(['Đỏ','Xanh','Trắng','Đen']),
            'cost'=>$this->faker->numberBetween(50, 200)
        ];
    }
}
